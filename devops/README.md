### Execução local do projeto, usando docker
- **script startDocker**
    - `docker build -t tcarvi:latest .`
    - `docker run --name tcarvi -p 8080:8080 tcarvi:latest`
  
### Clean das imagens e containers docker
- **script cleanDocker**
    - `docker stop tServer`
    - `docker rm tServer`
    - `docker rmi tserverimage`
    - `docker system prune --force`